# 'House Rental'
TestRPC

&nbsp;
## Part 5: AWS 

### 5.1. Truffle HDWallet Provider Private Key  

```
npm install truffle-hdwallet-provider-privkey
```

### 5.2. truffle.js AWS
```
$ vi truffle.js
```

< Your-Public-IP >
```javascript
const HDWalletProvider = require("truffle-hdwallet-provider-privkey");
const privateKeys = ["446fbba87648ed7cbfb410e1fdc97ceb8a79b8f69e5094a1befd9b248cdc9175"]; // private keys

module.exports = {
    networks: {
        development: {
            host: "127.0.0.1",
            port: 7545,
            network_id: "*" // Match any network id
        },
        awsNetwork: {
            provider: () => {
                return new HDWalletProvider(privateKeys, "http://<Your-Public-IP>:8545")
            },
            network_id: 15,
            gas: 2000000,
            gasPrice: 10000000000
        }
    }
};
```

### 5.3. AWS Geth
```
(Terminal #1) $ cd mission1; ./gethclient
(Terminal #2) $ cd mission1: ./gethconsole
```

### 5.4. Smart Contract를 AWS Blockchain
```
$ truffle migrate --network awsNetwork
```

```
Using network 'awsNetwork'.

Running migration: 1_initial_migration.js
  Deploying Migrations...
  ... 0x82a3aaf9d71857bfc5a8d5deeef594304925a5a9ca5b2af3ca5d626764692ffe
  Migrations: 0xb95d66c2d3b7605183c41ee5e5d775b83d0f8d5d
Saving successful migration to network...
  ... 0x96846c5fb00ecc90c1a675750631d8a4aee848f584500a3b325b2a425e29a423
Saving artifacts...
Running migration: 2_deploy_contracts.js
  Deploying LeaseProperty...
  ... 0xccf10953613ec5265fbada5691b46065064bdd0ab72ab2b6c1e9de4d3cc22608
  LeaseProperty: 0x98dd1dbdb6d3c6f9d9290a2e8671c1b53f843a6d
Saving successful migration to network...
  ... 0xfd7289eb789bee7639e0e3b7da52a894e3cb6bffa6730dfc7fa553ff292b2a4a
Saving artifacts...
```
> AWS Geth Smart Contract



&nbsp;

# And we're all done!
&nbsp;

&nbsp;

## [부록] 7. AWS Ethereum Template
### 7.1. AWS Blockchain Template Prerequisites 
 AWS Blockchain CloudFormation
- Create an Elastic IP Address
- Create a VPC and Subnets
- Create Security Groups
- Create an IAM Role for Amazon ECS and EC2 Instance Profile
- Create a Bastion Host

> AWS Ethereum Template  https://docs.aws.amazon.com/ko_kr/blockchain-templates/latest/developerguide/blockchain-template-getting-started-prerequisites.html

> AWS Ethereum Template  https://github.com/awslego/www/blob/master/data/HOL_BLOCKCHAIN_TEMPLATE.pdf


### 7.2. Truffle HDWallet Provider Private Key 

```
npm install truffle-hdwallet-provider-privkey
```

### 7.3. truffle.js 
```
$ vi truffle.js
```

```javascript
const HDWalletProvider = require("truffle-hdwallet-provider-privkey");
const privateKeys = ["afd2168f63635b5235cc8b4d69730faa4ffbea5cfcfab7b7d7625f91656e7d9f"]; // private keys

module.exports = {
    networks: {
        development: {
            host: "127.0.0.1",
            port: 7545,
            network_id: "*" // Match any network id
        },
        awsNetwork: {
            provider: () => {
                return new HDWalletProvider(privateKeys, "http://52.24.70.179:8082/private-ethereum-prd")
            },
            network_id: 1500,
            gas: 300000
        }
    }
};
```

### 7.4. Smart Contract를 AWS Blockchain
```
$ truffle migrate --network awsNetwork
```

```
Using network 'awsNetwork'.

Running migration: 1_initial_migration.js
  Deploying Migrations...
  ... 0xbf7fbfbd756c8c04125d3c3f408b4678834bd399d1b300f3c723d45ca8a2dde2
  Migrations: 0x0244afc4f2ccd12a4f7b5bc038ccc74962c96a57
Saving successful migration to network...
  ... 0x6d04ddaf491eeb04f3ccbc1e751e3033487ed156f703d2107cef8a12456b0c6d
Saving artifacts...
Running migration: 2_deploy_contracts.js
  Deploying LeaseProperty...
  ... 0x5f8c09ec0cd0420650b4721f17419f23d8001c96c6121fce64ac41a4bb39dc34
  LeaseProperty: 0xed32872236e066b1a20e051abfd378cc50457374
Saving artifacts...
```

### 7.6 House Rental 
 House Rental  http://52.24.70.179:8082
