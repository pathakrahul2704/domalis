module.exports = {
  url: process.env.DATABASE_URL || "postgres://pathak:123456789@localhost:5434/real_estate_database",
  options: { // "postgres://postgres:password@localhost:5432/real_estate_database",
    dialect: 'postgres',
    pool: {
      max: 5,
      min: 0,
      idle: 10000
    },
    logging: false
  }
};
